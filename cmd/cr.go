package cmd

import (
	"fmt"
	"log"
	"os"
	"strings"
	"text/template"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	ut "gitlab.com/zendrulat123/fsgen/cmd/ut"
)

// createCmd represents the create command
var createCmd = &cobra.Command{
	Use:     "cr",
	Short:   "Creates the config file so that your project has persistant data",
	Long:    `You need the config to allow for the rest of the commands to know about things (i.e. path, name, anything else you want)`,
	Example: `go run *.go cr -o :8082 -p /serv -n projectname -g gitlab.com/zendrulat123/serv -d "root:@tcp(127.0.0.1:3306)/ap"`,
	Run: func(cmd *cobra.Command, args []string) {
		path, _ := cmd.Flags().GetString("path")
		name, _ := cmd.Flags().GetString("name")
		db, _ := cmd.Flags().GetString("db")
		port, _ := cmd.Flags().GetString("port")
		g, _ := cmd.Flags().GetString("git")
		ut.CreateConfig()
		ut.WriteFile("con/persis.yaml")
		ut.ConfigAddEnv("con/persis.yaml", "env:")
		ut.ConfigAddFile("con/persis.yaml", " path: "+"\""+path+"/"+"\"")
		ut.ConfigAddFile("con/persis.yaml", " prj: "+"\""+name+"\"")
		ut.ConfigAddFile("con/persis.yaml", " db: "+"\""+db+"\"")
		ut.ConfigAddFile("con/persis.yaml", " port: "+"\""+port+"\"")
		ut.ConfigAddFile("con/persis.yaml", " git: "+"\""+g+"\"")
		viper.SetConfigName("persis") // config file name without extension
		viper.SetConfigType("yaml")
		viper.AddConfigPath(".")
		viper.AddConfigPath("./con/") // config file path
		viper.AutomaticEnv()          // read value ENV variable
		err := viper.ReadInConfig()
		if err != nil {
			fmt.Println("fatal error config file: default \n", err)
			os.Exit(1)
		}
		type Data struct {
			Model  string
			Port   string
			Dir    string
			Cmodel string
			Folder *os.File
		}
		Cm := strings.Title(name)
		d := Data{Model: name, Port: port, Dir: path, Cmodel: Cm}
		m, t := ut.CreateBase(path, name)
		// ? main.go file
		mtm := template.Must(template.New("queue").Parse(mainTemplate))
		err = mtm.Execute(m, d)
		if err != nil {
			log.Print("execute: ", err)
		}
		// ? template/index.html
		ttm := template.Must(template.New("queue").Parse(indexTemplate))
		err = ttm.Execute(t, d)
		if err != nil {
			log.Print("execute: ", err)
		}
		pleft := strings.Split(path, "/")[1]
		err, out, errout := ut.Shellout("cd " + pleft + " && go mod init " + g + " && go mod tidy && go mod vendor && go get -u github.com/gofiber/fiber/v2 && go get -u github.com/logrusorgru/aurora && go get -u github.com/rs/cors && go get -u gorm.io/gorm && go get -u gorm.io/driver/sqlite && go get github.com/go-sql-driver/mysql && go get gorm.io/driver/mysql && go get gorm.io/gorm  && go mod tidy && go mod vendor")
		if err != nil {
			log.Printf("error: %v\n", err)
		}
		fmt.Println(out)
		fmt.Println("--- errs ---")
		fmt.Println(errout)
		defer m.Close()
		defer t.Close()

	},
}

//main.go file
var mainTemplate = `

package main

import(
  "fmt"
)

func main(){
	fmt.Println()
}
  `

//template/index.html
var indexTemplate = `
 <p>this works</p>

  `

func init() {
	rootCmd.AddCommand(createCmd)
	createCmd.Flags().StringP("path", "p", "", "Set your path")
	createCmd.Flags().StringP("name", "n", "", "Set your project name")
	createCmd.Flags().StringP("db", "d", "", "Set your database source")
	createCmd.Flags().StringP("port", "o", "", "Set your port")
	createCmd.Flags().StringP("git", "g", "", "Set your git")
}
