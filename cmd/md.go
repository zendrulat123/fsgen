package cmd

import (
	"fmt"
	"log"
	"os"
	"strings"
	"text/template"

	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	ut "gitlab.com/zendrulat123/fsgen/cmd/ut"
)

// mdCmd represents the md command
var mdCmd = &cobra.Command{
	Use:     "md",
	Short:   "A brief description of your command",
	Long:    `running cmd`,
	Example: `go run *.go md -m "person" -p "name.string-jim age.int-33"`,
	Run: func(cmd *cobra.Command, args []string) {
		viper.SetConfigName("persis") // config file name without extension
		viper.SetConfigType("yaml")
		viper.AddConfigPath("con/") // config file path
		viper.AutomaticEnv()        // read value ENV variable
		err := viper.ReadInConfig()
		if err != nil {
			fmt.Println("fatal error config file: default \n", err)
			os.Exit(1)
		}
		path := viper.GetString("env.path")
		fn := viper.GetString("env.prj")
		model, _ := cmd.Flags().GetString("model")
		prop, _ := cmd.Flags().GetString("prop")
		ds := viper.GetString("env.db")
		//grabbed env vars and used them below
		getpath(path, fn, model, prop, ds)
	},
}

func getpath(path string, fn string, model string, prop string, ds string) {

	ut.CreateDB(path, fn)
	//get path for template execution
	pleft := strings.Split(path, "/")[1]
	type data struct {
		Model     string
		Prop      []string
		Propnodot []string
		Value     []string
		Ds        string
	}
	//installing imports
	err, out, errout := ut.Shellout("cd " + pleft + " && go mod init " + pleft + " && go mod tidy && go mod vendor && go get -u github.com/logrusorgru/aurora && go get -u github.com/rs/cors && go get -u gorm.io/gorm && go get -u gorm.io/driver/sqlite && go get github.com/go-sql-driver/mysql && go get gorm.io/driver/mysql && go get gorm.io/gorm")
	if err != nil {
		log.Printf("error: %v\n", err)
	}

	tm := template.Must(template.New("queue").Parse(queueTemplate))

	//loading template vars
	Propdatatype := ut.GetPropDatatype(prop)
	PropValue := ut.GetPropValue(prop)
	Tmodel := strings.Title(model)
	//the slicing/trimming for the object to work.
	ss := ut.SepProp(Propdatatype)
	pv, val := ut.SeparateCommaProp(PropValue)

	var d = data{Model: Tmodel, Prop: ss, Propnodot: pv, Value: val, Ds: ds}

	f, err := os.Create(pleft + "/db/" + "db" + fn + ".go")
	if err != nil {
		log.Println("create file: ", err)
		return
	}
	err = tm.Execute(f, d)
	if err != nil {
		log.Print("execute: ", err)
		return
	}

	err, outs, errouts := ut.Shellout(`cd ` + pleft + `&& gofmt && goimports && go build && go install && go mod tidy && go mod vendor`)
	if err != nil {
		log.Printf("error: %v\n", err)
	}

	fmt.Println(out)
	fmt.Println(outs)
	fmt.Println("--- errs ---")
	fmt.Println(errout)
	fmt.Println(errouts)
	//add the db's method to projectname file
	var dbfile = "db" + Tmodel + "()"
	fmt.Println(pleft + "/" + fn)
	err = ut.AppendStringToFile(pleft+"/"+fn, dbfile)
	if err != nil {
		log.Printf("error: %v\n", err)
	}

	f.Close()
}

var queueTemplate = `
  package main
  
  import (
	_ "github.com/go-sql-driver/mysql"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"fmt"
  )
  
  type {{.Model}} struct {
	id int
	{{range slice .Prop 0 }}
	{{.}} 
	{{end}}
  }


  func {{.Model}}DB(){

  db, err := gorm.Open(mysql.Open("{{.Ds}}"), &gorm.Config{})
  if err != nil {
	  panic("failed to connect database")
  }

  db.AutoMigrate(&{{.Model}}{})

  // Create
  result:=db.Create(&{{.Model}}{		
{{range $i, $prop := .Propnodot}}
{{$value := index $.Value $i}}
{{$prop}} {{$value}}
{{end}}

  })


fmt.Println({{.Model}}.id)            // returns inserted data's primary key
fmt.Println(result.Error)      // returns error
fmt.Println(result.RowsAffected)
}
  `

func init() {
	rootCmd.AddCommand(mdCmd)
	mdCmd.Flags().StringP("model", "m", "", "Set your model")
	mdCmd.Flags().StringP("prop", "p", "", "Set your properties")
}
