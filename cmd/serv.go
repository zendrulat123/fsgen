package cmd

import (
	"fmt"
	"log"
	"os"
	"strings"
	"text/template"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	ut "gitlab.com/zendrulat123/fsgen/cmd/ut"
)

var servCmd = &cobra.Command{
	Use:     "serv",
	Short:   "Creates the server file",
	Long:    `a`,
	Example: `go run *.go serv -m "person"`,

	Run: func(cmd *cobra.Command, args []string) {
		viper.SetConfigName("persis") // config file name without extension
		viper.SetConfigType("yaml")
		viper.AddConfigPath("con/") // config file path
		viper.AutomaticEnv()        // read value ENV variable
		err := viper.ReadInConfig()
		if err != nil {
			fmt.Println("fatal error config file: default \n", err)
			os.Exit(1)
		}
		path := viper.GetString("env.path")
		filename := viper.GetString("env.prj")
		model, _ := cmd.Flags().GetString("model")
		port := viper.GetString("env.port")
		git := viper.GetString("env.git")

		//port = viper.GetString("env.port")
		f := ut.CreateServ(path, filename)
		type data struct {
			Model  string
			Port   string
			Dir    string
			Cmodel string
			Folder *os.File
		}
		Cm := strings.Title(model)
		var d = data{Model: model, Port: port, Dir: path, Cmodel: Cm}
		pleft := strings.Split(path, "/")[1]
		ut.UpdateText(pleft+"/main.go", "fmt.Println()", pleft+"."+d.Cmodel+"()")
		ut.UpdateText(pleft+"/main.go", " \""+"fmt"+" \"", pleft+" \""+git+"/"+pleft)

		// ? add code to server file
		tm := template.Must(template.New("queue").Parse(serverTemplate))
		err = tm.Execute(f, d)
		if err != nil {
			log.Print("execute: ", err)
			return
		}
		err, out, errout := ut.Shellout("cd " + pleft + " && go mod init " + git + " && go mod tidy && go mod vendor")
		if err != nil {
			log.Printf("error: %v\n", err)
		}
		fmt.Println(out)
		fmt.Println("--- errs ---")
		fmt.Println(errout)
		defer f.Close()
	},
}

//main.go file
var mainT = `

package main

import(
  "fmt"
  "{{.Dir}}"

)

func main(){
	fmt.Println("works")
	{{.Dir}}.{{.Cmodel}}()
}
  `
var serverTemplate = `
package serv{{.Cmodel}}

import (
	"fmt"
	"log"
	"net/http"
	"html/template"
	"context"
	"io"
	"net/url"
	"time"

	//only 3rd parties
	. "github.com/logrusorgru/aurora"
	"github.com/rs/cors"

)

//used to get template stuff
var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}
var err error

// function
func {{.Cmodel}}() {


  //actual server
	mux := http.NewServeMux()
	mux.HandleFunc("/{{.Model}}", {{.Cmodel}}home)


	//used to get other files css/js
	fs := http.FileServer(http.Dir("static/"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	
	//used for keeping context and requests for the server for logging
	handler := cors.Default().Handler(mux)
	c := context.Background()
	log.Fatal(http.ListenAndServe("{{.Port}}", AddContext(c, handler)))

}

func {{.Cmodel}}home(w http.ResponseWriter, r *http.Request){
	//switch statement for get or post
	switch r.Method {

	case "GET":
		//go get html file
		err := tpl.ExecuteTemplate(w, "index.html", nil)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}

	case "POST":
		//go get html file
		err := tpl.ExecuteTemplate(w, "index.html", nil)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}
	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")
	}

}
var Start = time.Now()
var Durations = time.Now().Sub(Start)

//getting context
type Contexter struct {
	M      string
	U      *url.URL
	P      string
	B      io.ReadCloser
	Gb     func() (io.ReadCloser, error)
	Host   string
	Form   url.Values
	Cancel <-chan struct{}
	R      *http.Response
	H      http.Header
	D      time.Duration
	I      string
}

//used to shorten use of Contexter
var CC Contexter

//initializing context
func AddContext(ctx context.Context, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		Start := time.Now()
		Duration := time.Now().Sub(Start)

		CC = Contexter{
			M:      r.WithContext(ctx).Method,
			U:      r.WithContext(ctx).URL,
			P:      r.WithContext(ctx).Proto,
			B:      r.WithContext(ctx).Body,
			Host:   r.WithContext(ctx).Host,
			Form:   r.WithContext(ctx).Form,
			Cancel: r.WithContext(ctx).Cancel,
			R:      r.WithContext(ctx).Response,
			D:      Duration,
			H:      r.WithContext(ctx).Header,
			//I:      Clients.ReadUserIP(r),
		}

		fmt.Println(Blue("/ʕ◔ϖ◔ʔ/^^^^^^^^^^^^^^^^^^^^^^^"))
		fmt.Printf("Method:%s\n - Status:%s\n - URL:%s - Body:%v\n - Host:%s\n - Form:%v\n - Cancel:%d\n - Response:%d\n - Dur:%02d-00:00\n - Cache-Control:%s - Accept:%s\n - IP:%s\n",
			Cyan(CC.M),
			Brown(r.Header.Get("X-Forwarded-Port")),
			Red(CC.U),
			Blue(CC.B),
			Yellow(CC.Host),
			BgRed(CC.Form),
			BgGreen(CC.Cancel),
			BgBrown(CC.R),
			BgMagenta(CC.D),
			Red(CC.H.Get("Cache-Control")),
			Blue(CC.H.Get("Accept")),
			Yellow(CC.I),
		)
		//this is to spit out the ctx.header in pieces cause its exhaustive
		// for k, v := range CC.H {
		// 	fmt.Println("\n", k, v)
		// }
		cookie, _ := r.Cookie("username")

		if cookie != nil {
			//Add data to context
			ctx := context.WithValue(r.Context(), "Username", cookie.Value)
			next.ServeHTTP(w, r.WithContext(ctx))

		} else {

			if err != nil {
				// Error occurred while parsing request body
				w.WriteHeader(http.StatusBadRequest)

				return
			}
			next.ServeHTTP(w, r.WithContext(ctx))
		}
	})
}

  `

func init() {
	rootCmd.AddCommand(servCmd)
	servCmd.Flags().StringP("model", "m", "", "Set your model")
	servCmd.Flags().StringP("port", "t", "", "Set your port")
}
