package cmd

import (
	"bytes"
	"fmt"
	"log"
	"os/exec"

	"github.com/spf13/cobra"
)

// testCmd represents the test command
var testCmd = &cobra.Command{
	Use:   "test",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("test called")
		const ShellToUse = "bash"
		err, out, errout := Shellout(`go run *.go cr -o :8082 -p /serv -n projectname -g gitlab.com/zendrulat123/serv -d "root:@tcp(127.0.0.1:3306)/ap" && go run *.go md -m "person" -p "name.string-jim age.int-33" && go run *.go serv -m "person" && cd serv && go install && go build && gofmt -r '(a) -> a' -l *.go &&  go run *.go`)
		if err != nil {
			log.Printf("error: %v\n", err)
		}

		fmt.Println("--- print out ---")
		fmt.Println(out)
		fmt.Println("--- errs ---")
		fmt.Println(errout)
		// sudo /opt/lampp/lampp start
	},
}

func init() {
	rootCmd.AddCommand(testCmd)

}
func Shellout(command string) (error, string, string) {
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd := exec.Command("bash", "-c", command)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	return err, stdout.String(), stderr.String()
}
