/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"html/template"
	"log"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	ut "gitlab.com/zendrulat123/fsgen/cmd/ut"
)

// fibCmd represents the fib command
var fibCmd = &cobra.Command{
	Use:     "fib",
	Short:   "add a gofiber server",
	Long:    ``,
	Example: `go run *.go fib -m "person"`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("fib called")
		viper.SetConfigName("persis") // config file name without extension
		viper.SetConfigType("yaml")
		viper.AddConfigPath("con/") // config file path
		viper.AutomaticEnv()        // read value ENV variable
		err := viper.ReadInConfig()
		if err != nil {
			fmt.Println("fatal error config file: default \n", err)
			os.Exit(1)
		}
		path := viper.GetString("env.path")
		filename := viper.GetString("env.prj")
		model, _ := cmd.Flags().GetString("model")
		port := viper.GetString("env.port")
		git := viper.GetString("env.git")

		//port = viper.GetString("env.port")
		f := ut.CreateServ(path, filename)
		r := ut.CreateRoute(path, filename)
		type data struct {
			Model  string
			Port   string
			Dir    string
			Cmodel string
			Folder *os.File
		}
		Cm := strings.Title(model)
		var d = data{Model: model, Port: port, Dir: path, Cmodel: Cm}
		pleft := strings.Split(path, "/")[1]

		ut.UpdateText(pleft+"/main.go", "fmt.Println()", pleft+"."+d.Cmodel+"()")
		ut.UpdateText(pleft+"/main.go", "import(", "import ( "+pleft+" \""+git+"/"+pleft+"\"")

		// ? add code to server file
		tm := template.Must(template.New("queue").Parse(fiberserverTemplate))
		err = tm.Execute(f, d)
		if err != nil {
			log.Print("execute: ", err)
			return
		}
		// ? add code to server file
		fbr := template.Must(template.New("queue").Parse(fiberroute))
		err = tm.Execute(r, fbr)
		if err != nil {
			log.Print("execute: ", err)
			return
		}

		err, out, errout := ut.Shellout("cd " + pleft + " && go mod init " + git + " && go mod tidy && go mod vendor")
		if err != nil {
			log.Printf("error: %v\n", err)
		}
		fmt.Println(out)
		fmt.Println("--- errs ---")
		fmt.Println(errout)
		defer f.Close()
	},
}

var fiberserverTemplate = `
package serv{{.Cmodel}}

import (
	"fmt"
	"log"
	"net/http"
	"html/template"
	"context"
	"io"
	"net/url"
	"time"
	"github.com/rs/cors"

)
func {{.Cmodel}}() {
app := fiber.New()
app.Use(cors.New())
app.Use(pprof.New())
app.Server().MaxConnsPerIP = 1
app.Get("/", func(c *fiber.Ctx) error {
	return c.SendString("Nice just generated your project 👋!")
})
app.Listen("{{.Port}}")
}
  `
var fiberroute = `
package route{{.Cmodel}}

func SetupRoutes(app *fiber.App) {



}

`

func init() {
	rootCmd.AddCommand(fibCmd)
	fibCmd.Flags().StringP("model", "m", "", "Set your model")
	fibCmd.Flags().StringP("port", "t", "", "Set your port")
}
