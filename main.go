package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"

	"github.com/rs/cors"
	"gitlab.com/zendrulat123/fsgen/cmd"
)

func main() {
	cmd.Execute()
	//actual server
	mux := http.NewServeMux()
	mux.HandleFunc("/go", Gohome)
	//used to get other files css/js
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))

	//used for keeping context and requests for the server for logging
	handler := cors.Default().Handler(mux)

	log.Fatal(http.ListenAndServe(":8081", handler))

}

//template to get file
func Gohome(w http.ResponseWriter, r *http.Request) {
	fmt.Println(w.Header())
	fmt.Println("started")
	fmt.Println(r.Header.Get("Origin"))
	allowedHeaders := "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token"
	w.Header().Set("Access-Control-Allow-Origin", "http://localhost:8081/go")
	w.Header().Set("Access-Control-Allow-Origin", "http://localhost:8081")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
	w.Header().Set("Access-Control-Expose-Headers", "Authorization")
	w.WriteHeader(http.StatusOK)

	//switch statement for get or post
	switch r.Method {

	case "GET":
		//go get html file
		err := tpl.ExecuteTemplate(w, "index.html", nil)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}

	case "POST":
		//go get html file
		err := tpl.ExecuteTemplate(w, "index.html", nil)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}
	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")
	}

}

//used to get template stuff
var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}

var err error
