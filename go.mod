module gitlab.com/zendrulat123/fsgen

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/logrusorgru/aurora v2.0.3+incompatible
	github.com/mitchellh/go-homedir v1.1.0
	github.com/rs/cors v1.7.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
)
